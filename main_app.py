import asyncio
from aiohttp import web
import aioredis


async def get_data(key):
    ''' Get JSON string from Redis '''
    pool = await aioredis.create_pool(('localhost', 6379))
    try:
        async with pool.get() as redis:
            result = await redis.get(key)
    finally:
        pool.close()
        await pool.wait_closed()
    try:
        output = result.decode('utf8')
    except AttributeError:
        return
    return output


async def index(request):
    output = await get_data('true_detective:string')
    return web.Response(text=output)


async def season(request):
    key = 'true_detective:season{}:string'.format(request.match_info['id'])
    output = await get_data(key)
    if output:
        return web.Response(text=output)
    raise web.HTTPNotFound()


async def episode(request):
    key = 'true_detective:season{s_id}:episode{e_id}:string'.format(
        s_id=request.match_info['s_id'],
        e_id=request.match_info['e_id']
        )
    output = await get_data(key)
    if output:
        return web.Response(text=output)
    raise web.HTTPNotFound()


def init_func(argv):
    app = web.Application()
    app.router.add_route(method='GET', path='/', handler=index, name='index')
    app.router.add_route(method='GET', path='/season/{id}', handler=season)
    app.router.add_route(method='GET', path='/season/{s_id}/episode/{e_id}', handler=episode)
    return app
