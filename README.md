# True Detective API on aiohttp

Простая API с информацией по сериалу [True Detective](http://www.hbo.com/true-detective/)

### Зависимости

- Python =< 3.5
- Redis


### Установка

0) Для работы API, в системе должен быть корректно установлен [Redis](http://redis.io/) и работать по адресу localhost:6379 без авторизации. Он будет использоваться, как база данных.

1) создаем виртуальное окружение и устанавливаем зависимости из файла `requirements.txt`
```sh
virtualenv --python=python3 .env
source .env/bin/activate
pip install -r requirements.txt
```

2) Экспортируем данные в Redis. Для этого заходим в папку `source` и запускаем файл `export_data_to_redis.py` командой:
```sh
python export_data_to_redis.py
```

из-под активированного виртуального окружения. Или скопируйте дамп редиса `source/dump.rdb` в папку `/var/lib/redis`:


3) Запускаем сервер:
```sh
python -m aiohttp.web main_app:init_func
```

### Использование

http://0.0.0.0:8080/ - общая информация о сериале

http://0.0.0.0:8080/season/1 - информация о первом сезоне

http://0.0.0.0:8080/season/2 - информация о втором сезоне

http://0.0.0.0:8080/season/1/episode/1 - информация о первом эпизоде первого сезона

http://0.0.0.0:8080/season/2/episode/4 - информация о четвертом эпизоде второго сезона