import asyncio
import aioredis
import glob


async def save_main_page():
    file = 'true_detective.json'
    with open(file) as f:
        data = f.read()
        await save_key('true_detective:string', data)


async def save_seasons():
    seasons_files = ['season1.json', 'season2.json']
    for i, file in enumerate(seasons_files):
        with open(file) as f:
            key = 'true_detective:season{}:string'.format(i+1)
            data = f.read()
            await save_key(key, data)


async def save_key(key, value):
    pool = await aioredis.create_pool(('localhost', 6379))
    try:
        async with pool.get() as redis:
            result = await redis.set(key, value)
    finally:
        pool.close()
        await pool.wait_closed()


async def main():
    await save_main_page()
    await save_seasons()
    files = glob.glob('s??e??.json')
    for file in files:
        with open(file) as f:
            season = file[2]
            episode = file[5]
            key = 'true_detective:season{s_id}:episode{e_id}:string'.format(s_id=season, e_id=episode)
            data = f.read()
            await save_key(key, data)


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
